<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('halaman.register');
    }

    public function welcome(Request $request)
    {
        $nama1 = $request->namadepan;
        $nama2 = $request->namabelakang;

        return view('halaman.welcome' , compact('nama1','nama2'));
    }
}