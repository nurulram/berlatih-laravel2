@extends('layout.master')
@section('judul')
    Halaman Register
@endsection

@section('content')
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label> <br> <br>
        <input type="text" name="namadepan"> <br> <br>
        <label>Last name:</label> <br> <br>
        <input type="text" name="namabelakang"> <br> <br>
        <label>Gender:</label> <br> <br>
        <input type="radio" name="G" >Male <br>
        <input type="radio" name="G" >Female <br>
        <input type="radio" name="G" >Other <br> <br>
        <label>Nationality:</label> <br> <br>
        <select name=JK>
            <option value="Indonesian"> Indonesian</option>
            <option value="Singaporean"> Singaporean</option>
            <option value="Malaysian"> Malaysian</option>
            <option value="Australian"> Australian</option>
        </select> <br> <br>
        <label>Language Spoken:</label> <br> <br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br> <br>
        <label>Bio:</label> <br> <br>
        <textarea name="Bio" id="" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up"/>
    </form>
    
@endsection